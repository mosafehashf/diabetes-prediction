-include .env
export

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

pre-commit: ## Runs the pre-commit over entire repo
	@poetry run pre-commit run --all-files

build-image: ## Build the images locally
	@docker build -f "./containers/Dockerfile" --tag ${PIPELINE_IMAGE_NAME} .

push-image: ## Build image and push to GCR
	@ $(MAKE) build-image && \
	docker push ${PIPELINE_IMAGE_NAME}

compile: ## complie the pipeline into a json file
	@poetry run python -m src.pipeline

run: ## running
	@ $(MAKE) compile && \
	poetry run python -m src.trigger
